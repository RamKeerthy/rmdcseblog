# RMD BLOG SERVER

1. The blog server an android based application is used by HOD of Computer Science and Engineering to post regular updates.
2. This application supports photo upload from file system of android or allows the user to take a photo and upload too.
3. The uploaded image will be stored in firebase server and will be sent to RMD CSE BLOG application installed by students.

### Note: This application uses firebase server and related API to upload images to firebase server