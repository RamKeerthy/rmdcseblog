package com.example.user.rmdblogserver;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class MainActivity extends AppCompatActivity {

    private ImageButton mSelectImage;
    private EditText mTitleField;
    private EditText mDescField;
    private Button mSubmitButton;

    private Uri mImageUri = null;

    private static final int GALLERY_REQUEST = 1;

    private StorageReference mStorage;
    private DatabaseReference mDatabase;


    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Blog");

        mSelectImage = (ImageButton) findViewById(R.id.ImageSelect);
        mTitleField = (EditText) findViewById(R.id.TitleField);
        mDescField = (EditText) findViewById(R.id.DescField);
        mSubmitButton = (Button) findViewById(R.id.SubmitButton);

        mProgress = new ProgressDialog(this);

        mSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("*/*");
                startActivityForResult(galleryIntent, GALLERY_REQUEST);

            }
        });

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startPosting();

            }
        });

        Toast.makeText(this, mStorage.toString(), Toast.LENGTH_SHORT).show();
    }
    private void startPosting(){

        mProgress.setMessage("Posting to Blog...");


        final String title_Val = mTitleField.getText().toString().trim();
        final String desc_Val = mDescField.getText().toString().trim();

        if(!TextUtils.isEmpty(title_Val) && !TextUtils.isEmpty(desc_Val) && mImageUri != null){

            mProgress.show();
            Toast.makeText(this, "Inside if...", Toast.LENGTH_SHORT).show();

            Toast.makeText(this,mImageUri.getLastPathSegment(), Toast.LENGTH_SHORT).show();

            StorageReference filepath = mStorage.child("Blog").child(mImageUri.getLastPathSegment());

            filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    Uri downloadUrl = taskSnapshot.getDownloadUrl();

                    DatabaseReference newPost = mDatabase.push();

                    newPost.child("title").setValue(title_Val);
                    newPost.child("desc").setValue(desc_Val);
                    newPost.child("image").setValue(downloadUrl.toString());

                    mProgress.dismiss();

                }
            });
            Toast.makeText(this, filepath.toString(), Toast.LENGTH_SHORT).show();
            //mProgress.dismiss();

        }
        else{

            Toast.makeText(this, "Fields are empty", Toast.LENGTH_SHORT).show();

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){

            mImageUri = data.getData();

            mSelectImage.setImageURI(mImageUri);
        }
    }
}
